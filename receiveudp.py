#!/usr/bin/python3
#
import ROOT
from ROOT import gROOT, TCanvas, TF1, TH1F, THttpServer, TGraph, TDatime
from ROOT import gROOT, gDirectory, gPad
import time
import datetime
from random import randint
import os


def get_root_date():
    spy=datetime.datetime.now()
    #print("D... now== ",spy)
    start=TDatime(spy.year,spy.month,spy.day,
                  spy.hour, spy.minute,spy.second)
    startRO=start.Convert()
    print("D... get_root_date:",spy, start.GetDayOfWeek(), startRO)
    return startRO


def new_graph( name ,color=1 , linewidth=2):
    g1=TGraph()
    g1.SetName(name)
    g1.SetTitle( name )
    startRO=get_root_date()
    x=startRO
    #
    #g1.SetPoint(0, x, 25)
    #
    g1.GetXaxis().SetTimeDisplay(1)
    g1.GetXaxis().SetNdivisions(503)
    #g1.GetXaxis().SetTimeFormat("%m-%d %H:%M")
    g1.GetXaxis().SetTimeFormat("%a/%H:%M")
    g1.GetXaxis().SetTimeOffset(0)
    # dont work!
    g1.SetMarkerStyle(21)
    g1.SetLineStyle(1)
    g1.SetLineWidth( linewidth )
    g1.SetLineColor( color )
    #######g1.SetGridX()
    return g1


def set_limits(g):
    #if len(ally)>0:
    #    print( len(ally), "max:", max(ally) , "x:", x , "last:" , ally[-1] )
    #print( "D... point",g1.GetN(), ally[i] )
    g.GetXaxis().SetLimits( x-ROOT.TIMEBUFFER, x+10)
    #
    #
    #
    ally=g.GetY()
    mx,mn=max(ally),min(ally)
    rank=abs(mx-mn)
    if rank==0: rank=0.001
    mx,mn=mx+rank*0.1, mn-rank*0.1
    if mn!=mx:
        g.SetMaximum( mx )
        g.SetMinimum( mn )
        g.GetYaxis().SetLimits( mn,mx )
        g.GetHistogram().GetYaxis().SetRangeUser(mn,mx)
    #print( "                             ",mn," - ",mx)
    #print( gPad )
    #if not gPad==ROOT.NULL:
    #    gPad.SetGridx(1)
    #    gPad.SetGridy(1)

    
# i dont know, it seemed to work on canvas
#g1.GetXaxis().SetLimits( startRO, startRO+3600)
#g1.Draw("paw")
g1=new_graph( "temp1", 2)
g2=new_graph( "GasCell", 2 ,4 )
g3=new_graph( "Probe1", 3)
g4=new_graph( "Probe4", 4)
g5=new_graph( "Cross", 8) # 5 yellow NOT
g6=new_graph( "Probe2", 6)

h1=TH1F("h1","UDP samples",4000,0,4000)
#gDirectory.Add( g1 )
#gDirectory.Add( g1 )

t=ROOT.TLatex()
t.SetTextSize(60)
t.SetName("TEXTS")
t.SetText(0,0,"HALO =====")

# CARES ABOUT TIMEBUFFER ONLY
gROOT.LoadMacro("SomeFunction.C")
print( ROOT.TIMEBUFFER )

serv=THttpServer("http:0.0.0.0:8080")
#serv.Register("/", h1);
serv.Register("/", g1);
serv.Register("/", g2);
serv.Register("/", g3);
serv.Register("/", g4);
serv.Register("/", g5);
serv.Register("/", g6);
serv.Register("/", t);

#// enable monitoring and
#// specify items to draw when page is opened
serv.SetItemField("/","_monitoring","5000");
serv.SetItemField("/","_layout","ver231");
serv.SetItemField("/","_drawitem","[temp1,GasCell,Cross,Probe4,Probe2,TEXTS]");
serv.SetItemField("/","_drawopt","l");

serv.RegisterCommand("/DoSomething","SomeFunction()");
serv.RegisterCommand("/Buffer1m", "TimeBuffer1m()");
serv.RegisterCommand("/Buffer5m", "TimeBuffer5m()");
serv.RegisterCommand("/Buffer20m","TimeBuffer20m()");
serv.RegisterCommand("/Buffer1h","TimeBuffer1h()");
serv.RegisterCommand("/Buffer12h","TimeBuffer12h()");
serv.RegisterCommand("/Buffer48h","TimeBuffer48h()");




# Again we import the necessary socket python module
import socket
# Here we define the UDP IP address as well as the port number that we have
# already defined in the client python script.
UDP_IP_ADDRESS = "127.0.0.1"
UDP_IP_ADDRESS = "0.0.0.0"
UDP_PORT_NO = 7777

# declare our serverSocket upon which
# we will be listening for UDP messages
serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# One difference is that we will have to bind our declared IP address
# and port number to our newly declared serverSock
serverSock.bind((UDP_IP_ADDRESS, UDP_PORT_NO))

i=0
while True:
    # DECODE UDP LINE
    data, addr = serverSock.recvfrom(1024)
    print( "Message: ", data, "addr=", addr)
    #
    vals=data.decode("utf8").split(" ")
    with open( os.path.expanduser("~/gascell.log"),"a") as f:
        f.write( datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S  ")+data.decode("utf8")+ "\n" )
    ok=False
    try:
        vals=[ float(x) for x in vals ]
        if len(vals)>=6:
            ok=True
        # sometims UDP makes a mess....    
        for j in range(5):
            if vals[1+j]>1.5: ok=False
    except:
        print("not floats or bad length")
    if not ok: continue
        #
    #========================== DECODED =======
    h1.SetBinContent( i , vals[0] )
    i+=1
    #x=TDatime(2008,2,28,15,52,00).Convert() + i
    #
    #=============================================
    #
    x=get_root_date()
    g1.SetPoint( g1.GetN(), x,vals[0] )
    g2.SetPoint( g2.GetN(), x,vals[1] )
    g3.SetPoint( g3.GetN(), x,vals[2] )
    g4.SetPoint( g4.GetN(), x,vals[3] )
    g5.SetPoint( g5.GetN(), x,vals[4] )
    g6.SetPoint( g6.GetN(), x,vals[5] )


    set_limits( g1  )
    set_limits( g2  )
    set_limits( g3  )
    set_limits( g4  )
    set_limits( g5  )
    set_limits( g6  )

    mbar="{}".format(vals[1] )
    cels="{}".format(vals[0] )
    t.SetText(0.08,0.3, "#frac{ "+mbar+" mbar }{ "+cels+" #circ C}" ) 
    

    
    
